var utils  = require("loader-utils");
var Source = require('webpack-sources');
var cache  = {};

/**
 * The main loader entry point
 *
 * The loader accespts the raw content, but return
 * just the bogus javascript (exprots hash of the content passed).
 *
 * Later, at emmit time (after the compiler was satisfied with our js code)
 * it switches out that bogus javascript with the real content.
 *
 * Result = Assets emmited without problems! :)
 *
 * @param  {buffer/string} content - Raw asset content
 * @return {string}        Bogus javascript to fool the compiler
 *
 * @exports {function} StylusLoader
 * @since 0.1.0
 */
module.exports = function(content)
{
  this.cacheable && this.cacheable(true);
	
	var file = utils.parseQuery(this.query).entry;
	var hash = utils.getHashDigest(content);
	
	this._compiler.plugin('emit',(compilation,callback) => {
  	
  	if (compilation.assets[file])
  	compilation.assets[file] = new Source.RawSource (content);
  	
    cache[file] = hash;
  	callback ();
  	
	});
	
	return 'module.exports = ' + JSON.stringify(hash) + ';';
}

/**
 * Register that we don't mind having a buffer passed
 * @exports {boolean} raw
 */
module.exports.raw = true;