var utils  = require('loader-utils');
var merge  = require('lodash/merge');
var crypto = require('crypto');
var jade   = require('jade');

/**
 * Cache object (old system)
 * @var {object} cache
 */
var cache = {};

/**
 * The main loader entry point
 *
 * @param  {buffer/string} content - Raw Jade content (hopefully)
 * @return {string}        Rendered HTML string
 *
 * @exports {function} JadeLoader
 * @since 0.1.0
 */
module.exports = function JadeLoader (content)
{
  this.cacheable && this.cacheable(true);
  
  var resource = this.resourcePath;
  var compiler = this._compiler;
  var options  = this._compilation.options;
  
  var defaults = {
    configure: (c) => {return c},
    pretty:    this.minimize ? false : true
  };
  
  var config = merge(defaults,utils.getLoaderConfig(this,'jade'));
      config.filename = resource;
      config = config.configure(config);
      
  var temp = jade.compileClientWithDependenciesTracked(content,config);
  var deps = temp.dependencies;
      deps = deps.filter((f,i) => deps.lastIndexOf(f) == i);
  
  deps.forEach (this.addDependency.bind(this));
  deps.push    (resource);
  
//  if (!cache[resource]) cache[resource] = { stamps:'',content:'' };
  
//  var stamps = crypto.createHash('sha256').update(JSON.stringify(
//    deps.reduce((o,f) => { o[f] = compiler.fileTimestamps[f]; return o; },{})
//  )).digest('utf8');
  
//   if (cache[resource].stamps !== stamps) {
//     console.log('Rendering jade...');
//     cache[resource].content = jade.render(content,config);
//   } else {
//     cache[resource].content;
//   }
  
//  cache[resource].stamps = stamps;

  return jade.render(content,config);
}

/**
 * Register that we don't mind having a buffer passed
 * @exports {boolean} raw
 */
module.exports.raw = true;