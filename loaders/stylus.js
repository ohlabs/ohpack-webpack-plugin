var utils  = require('loader-utils');
var merge  = require('lodash/merge');
var crypto = require('crypto');
var Stylus = require('stylus');

/**
 * Cache object (old system)
 * @var {object} cache
 */
var cache = {};

/**
 * The main loader entry point
 *
 * @param  {buffer/string} content - Raw Stylus content (hopefully)
 * @return {string}        Rendered CSS string
 *
 * @exports {function} StylusLoader
 * @since 0.1.0
 */
module.exports = function StylusLoader (content)
{
  this.cacheable && this.cacheable(true);
	
  var resource = this.resourcePath;
  var compiler = this._compiler;
  var options  = this._compilation.options;
  
  var defaults = {
    setup:     (s) => {return s},
    configure: (c) => {return c},
    sourcemap: this.minimize ? false : { inline:true },
    compress:  this.minimize ? true  : false,
    paths: []
  };
  
  var config   = merge(defaults,utils.getLoaderConfig(this,'stylus'));
      config   = config.configure(config);
      
  var stylus   = Stylus(content.toString(),config).set('filename',resource);
      stylus   = config.setup(stylus);
      
  var deps     = stylus.deps();
  var done     = this.async();
  
  deps.forEach (this.addDependency.bind(this));
  deps.push    (resource);

//  if (!cache[resource]) cache[resource] = { stamps:'',content:'' };
  
//  var stamps = crypto.createHash('sha256').update(JSON.stringify(
//    deps.reduce((o,f) => { o[f] = compiler.fileTimestamps[f]; return o; },{})
//  )).digest('utf8');
  
//  if (cache[resource].stamps !== stamps) {
//    console.log('Rendering stylus...');
    stylus.render((err,css) => {
      if (err) return done(err);
      done(null,css); 
    });
//  } else {
//    done(null,cache[resource].content);
//  }
  
//  cache[resource].stamps = stamps;
}

/**
 * Register that we don't mind having a buffer passed
 * @exports {boolean} raw
 */
module.exports.raw = true;