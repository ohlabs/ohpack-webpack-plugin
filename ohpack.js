var merge  = require('lodash/merge');
var argv   = require('minimist')(process.argv.slice(2));
var isO    = require('lodash/isObject');
var wp     = require('webpack');

/**
 * OhPack plugins
 * @var {object} plugins
 */
var plugins = {
  logger: require('./plugins/logger')
};

/**
 * OhPack loaders
 * @var {object} loaders
 */
var loaders = {
  stylus: require.resolve('./loaders/stylus'),
  asset:  require.resolve('./loaders/asset'),
  jade:   require.resolve('./loaders/jade')
};

/**
 * Default OhPack config
 * @var {object} defaults
 */
var defaults = {
  logger: true,
  asset:  true,
  stylus: false,
  jade:   false
};

/**
 * Internal cache
 * (for eliminating unchanged files from the emit)
 * @var {object} cache
 */
var cache = {};

/**
 * OhPack plugin
 *
 * Does some of the boilerplate things
 * so our webpack.config.js file looks nicer
 *
 * Main features:
 * - Injects plugins for production packaging 
 * - Prepends loaders (assetloader + appropriate type loader)
 *   to registered entry points, but only to asset files (non-js)
 *
 * @constructor
 * @since 0.1.0
 */
function OhPack (conf)
{
  this.conf = isO(conf)
  ? merge({},defaults,conf)
  : merge({},defaults);
}

/**
 * Give public access to ohpack
 * customized loaders
 * @var {object} OhPack.loaders
 */
OhPack.loaders = loaders;

/**
 * Give public access to ohpack
 * customized plugins
 * @var {object} OhPack.plugins
 */
OhPack.plugins = plugins;

/**
 * The main plugin
 * entry method
 *
 * @param {Compiler} compiler - Webpack compiler instance
 *
 * @since 0.1.0
 */
OhPack.prototype.apply = function (compiler)
{
  var options = compiler.options;
  var entry   = compiler.options.entry;
  
  // Inject the appropriate loaders along
  // with the asset loader
  
  Object.keys(entry).forEach((key) => {
  
    if (/\.js$/.test(key))
      return entry[key];
    
    if (/\.(styl|stylus)$/.test(entry[key]) && this.conf.stylus)
      entry[key] = loaders.stylus+'!'+entry[key];
    
    if (/\.(jade|pug)$/.test(entry[key])    && this.conf.jade)
      entry[key] = loaders.jade+'!'+entry[key];
    
    entry[key] = (this.conf.asset
    ? (loaders.asset + '?entry=' + key + '!')
    : '') + entry[key];
    
  });
  
  // EXPREIMENTAL - MIGHT REMOVE NEEDED EMITS
  // See to remove unnececery emits
  // from the asset object during the emit phase so they wont be
  // displayed and wont confuse what was actually compiled
  
  compiler.plugin('emit',(compilation,cb) => {
    
    var newcache = Object
    .keys(compilation.records.chunks.byName)
    .reduce((object,name) => {
      
      var id = 'c'+compilation.records.chunks.byName[name];
      
      object[name] = {
        id:   id,
        hash: compilation.cache[id].hash
      };
      
      return object;
      
    },{});
    
    Object.keys(newcache).forEach((name) => {
      
      if (!cache[name]) return;
      if ( cache[name].id   !== newcache[name].id  ) return;
      if ( cache[name].hash !== newcache[name].hash) return;
      
      delete compilation.assets[name];
      
    });
    
    cache = newcache;
    
    cb();
    
  });
  
  // Use the OhPack Logger plugin
  // if required so by the user
  
  if (this.conf.logger) this.logger = new plugins.logger().apply(compiler);
  
  // Inject other plugins depending on the environment
  
  if (argv.p) {
    
    options.devtool = false;
    
    var definer = new wp.DefinePlugin({
      'process.env': { NODE_ENV:'"production"' }
    }).apply(compiler);
    
  } else {
    
    options.devtool = 'inline-source-map';
    
    var definer = new wp.DefinePlugin({
      'process.env': { NODE_ENV:'"development"' }
    }).apply(compiler);
    
  }
}

module.exports = OhPack;