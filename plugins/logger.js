var columnify = require('columnify');
var padstart  = require('lodash/padStart');
var colors    = require('colors');

/**
 * Units map for the bytes formatter
 * @var {array} UNITS
 */
var UNITS = [

  ['GB',1073741824],
  ['MB',1048576],
  ['KB',1024],
  [' B',1],
  [' B',0]

];

/**
 * Format passed bytes
 * to GB - MB - KB -  B
 *
 * @param  {number} bytes - Number of bytes to format
 * @return {string} String of formated size
 *
 * @since 0.1.0
 */
function formatBytes (bytes)
{
  var i = -1, size;
  do { i++, size = bytes/UNITS[i][1] } while (size < 1 && i < UNITS.length);
  return (isNaN(size) ? 0 : size.toFixed(2)) + ' ' + UNITS[i][0].dim
}

/**
 * Format milliseconds
 * to time passed
 *
 * @param  {number} ms - Number of milliseconds to format
 * @return {string} Formated time (MM:SS)
 *
 * @since 0.1.0
 */
function formatTime (ms)
{
  var m =  (ms/1000/60) << 0
    , s = ((ms/1000)    << 0) % 60;
  return padstart(m.toString(),2,'0')+':'+padstart(s.toString(),2,'0');
}

/**
 * The logger plugin
 * awesome in every way
 *
 * Main features:
 * - Displays time since yor last build
 * - Gives a clear view of the emitted assets
 * - Looks amasing
 *
 * @since 0.1.0
 */
function Logger () {}

/**
 * The main plugin entry
 * method
 *
 * @param {Compiler} compiler - Webpack compiler instance
 *
 * @since 0.1.0
 */
Logger.prototype.apply = function (compiler)
{
  compiler.plugin('done',(s) => { s.toString = this.log.bind(this,s) });

  var date = new Date ();

  if (compiler.options.watch) {

    this.startTimer();

    compiler.plugin('compile',function () {
      this.stopTimer();
      process.stderr.write('\r'+' ...  ');
    }.bind(this));

    compiler.plugin('done',function () {
      process.stderr.write('\r'+this.stamp()+' ');
      this.startTimer();
    }.bind(this));

  }

  return this;
}

Logger.prototype.stopTimer = function ()
{
  clearInterval(this.timer);
}

Logger.prototype.startTimer = function ()
{
  if (!this.check) this.check = new Date().getTime();
  this.timer = setInterval(function(){
    process.stderr.write('\r'+this.stamp()+' ');
  }.bind(this),1000);
}

/**
 * Print the time stamp to the console
 * with "\r" character (rewrite)
 *
 * @since 0.1.0
 */
Logger.prototype.stamp = function (reset)
{
  var now = new Date().getTime();
  var elapsed = now - this.check;
  if (reset) this.check = now;
  if (elapsed > 1*60*60*1000)
  { return 'HOURS' }
  else
  { return formatTime(elapsed) }
}

/**
 * Logs out the stats object
 *
 * @param {Stats}  statc   - The stat object produced by the compiler
 * @param {object} options - Options passed from the Webpack environment
 *
 * @since 0.1.0
 */
Logger.prototype.log = function (stats,options)
{
  var data   = stats.toJson(options,true);
  var assets = data.assets;

  var columns = columnify (assets,{

    columns: ['name','size'],
    showHeaders: false,
    columnSplitter: ' | '.dim,
    config: {
      name: {
        dataTransform: (d) => d.green,
        align:         'left'
      },
      size: {
        dataTransform: (d) => formatBytes(d),
        align:         'right'
      }
    }

  }).split('\n').map(l => '--:-- '.dim + l).join('\n');

  return [
    '\r'+this.stamp(true).dim,(data.hash+' in '+data.time+' ms').dim,
    '\n'+columns,
    (data.errors.length ? '\n'+data.errors.join('\n') : '')
  ].join(' ');
}

/**
 * @exports Logger
 */
module.exports = Logger;
